def dot_calculator(input)
  array = input.split(' ')
  return 'Invalid Input' if array.size != 3 || !['+', '-', '*', "//"].include?(array[1])

  first_element, operator, second_element = array
  operator = '/' if operator == '//'

  result_integer = first_element.size.send(operator, second_element.size)
  result_integer = 0 if result_integer.negative?

  response = ''
  result_integer.times{ response << '.' }
  response
end

puts ".. + ... (2 + 3)"
puts "#{dot_calculator(".. + ...")} (#{dot_calculator(".. + ...").size})"
